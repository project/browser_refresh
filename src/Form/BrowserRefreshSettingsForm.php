<?php

namespace Drupal\browser_refresh\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure browser-refresh settings for this site.
 */
class BrowserRefreshSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'browser_refresh_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['browser_refresh.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('browser_refresh.settings');

    $form['enable'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => $config->get('enable'),
    );
    $form['indicator_location'] = array(
      '#type' => 'select',
      '#title' => $this->t('Location of the indicator'),
      '#default_value' => $config->get('indicator_location'),
      '#options' => array(
        'br' => $this->t('Bottom right'),
        'bl' => $this->t('Bottom left'),
        'tr' => $this->t('Top right'),
        'tl' => $this->t('Top left'),
      ),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $config = $this->configFactory()->getEditable('browser_refresh.settings');

    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
