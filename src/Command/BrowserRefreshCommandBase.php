<?php

namespace Drupal\browser_refresh\Command;

use Drupal\browser_refresh\BrowserRefreshService;
use Drupal\Console\Core\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\browser_refresh\BrowserRefreshServiceInterface;

/**
 * Class BrowserRefreshCommandBase.
 *
 * @package Drupal\browser_refresh
 */
abstract class BrowserRefreshCommandBase extends Command {

  /**
   * @var \Drupal\browser_refresh\BrowserRefreshService
   */
  protected $service;

  /**
   * BrowserRefreshCommandBase constructor.
   *
   * @param \Drupal\browser_refresh\BrowserRefreshService $service
   */
  public function __construct(BrowserRefreshService $service) {
    parent::__construct();
    $this->service = $service;
  }

  /**
   * @param OutputInterface $output
   * @return BrowserRefreshServiceInterface
   */
  protected function getService($output): BrowserRefreshServiceInterface {
    return $this->service->setOutput($output);
  }

}
