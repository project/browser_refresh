<?php

namespace Drupal\browser_refresh;

/**
 * Interface BrowserRefreshServiceInterface.
 *
 * @package Drupal\browser_refresh
 */
interface BrowserRefreshServiceInterface {

  /**
   * @param $output
   * @return \Drupal\browser_refresh\BrowserRefreshServiceInterface
   */
  public function setOutput($output): BrowserRefreshServiceInterface;

  /**
   * @param bool $display
   * @return bool
   */
  public function isActive($display = FALSE): bool;

  /**
   * @return int|bool
   */
  public function getPid();

  /**
   * @return void
   */
  public function start(): void;

  /**
   * @return void
   */
  public function stop(): void;

  /**
   * @return void
   */
  public function restart(): void;

}
