<?php

namespace Drupal\browser_refresh\Commands;

use Drupal\browser_refresh\BrowserRefreshService;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile for browser_refresh.
 */
class BrowserRefreshCommands extends DrushCommands {

  /**
   * @var \Drupal\browser_refresh\BrowserRefreshService
   */
  protected $service;

  /**
   * BrowserRefreshCommands constructor.
   *
   * @param \Drupal\browser_refresh\BrowserRefreshService $service
   */
  public function __construct(BrowserRefreshService $service) {
    parent::__construct();
    $this->service = $service;
  }

  /**
   * Start browser refresh and keep it running.
   *
   *
   * @command browser:refresh-start
   * @aliases brstart,browser-refresh-start
   */
  public function refreshStart(): void {
    $this->service->start();
  }

  /**
   * Stop browser refresh.
   *
   *
   * @command browser:refresh-stop
   * @aliases brstop,browser-refresh-stop
   */
  public function refreshStop(): void {
    $this->service->stop();
  }

  /**
   * Get the status of browser refresh.
   *
   *
   * @command browser:refresh-status
   * @aliases brstatus,browser-refresh-status
   */
  public function refreshStatus(): void {
    $this->service->isActive(TRUE);
  }

  /**
   * Stop and start browser refresh and keep it running.
   *
   *
   * @command browser:refresh-restart
   * @aliases brrestart,browser-refresh-restart
   */
  public function refreshRestart(): void {
    $this->service->restart();
  }

}
