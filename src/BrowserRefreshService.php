<?php

namespace Drupal\browser_refresh;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class BrowserRefreshService.
 *
 * @package Drupal\browser_refresh
 */
class BrowserRefreshService implements BrowserRefreshServiceInterface {

  use StringTranslationTrait;

  /* @var \Symfony\Component\Console\Output\OutputInterface $output */
  private $output;

  /**
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * BrowserRefreshService constructor.
   *
   * @param \Drupal\Core\File\FileSystem $file_system
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(FileSystem $file_system, MessengerInterface $messenger, ConfigFactoryInterface $config_factory) {
    $this->fileSystem = $file_system;
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function setOutput($output): BrowserRefreshServiceInterface {
    $this->output = $output;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive($display = FALSE): bool {
    $active = (bool) $this->getPid();
    if ($display) {
      /* @var \Drupal\Core\StringTranslation\TranslatableMarkup $msg */
      if ($active) {
        $msg = $this->t('Browser Refresh is running (pid=:pid).', array(':pid' => $this->getPid()));
      }
      else {
        $msg = $this->t('Browser Refresh is not running.');
      }
      $this->displayMessage($msg->render(), ($active ? 'status' : 'error'));
    }
    return $active;
  }

  /**
   * {@inheritdoc}
   */
  public function getPid() {
    $config = $this->configFactory->getEditable('browser_refresh.settings');
    $pid = $config->get('pid');
    if ($pid) {
      $result = $this->exec('ps --cols=999 -lFp ' . $pid, 1);
      if (strpos($result, 'browser-refresh')) {
        return $pid;
      }
      $config->set('pid', 0)->save(TRUE);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function start(): void {
    // Check if browser-refresh is already running
    if ($this->isActive()) {
      $this->displayMessage('Browser Refresh already started.', 'warning');
      return;
    }

    // Install or update packages
    $this->exec('npm update');

    // Check if browser-refresh is installed
    $executable = DRUPAL_ROOT . '/' . drupal_get_path('module', 'browser_refresh') . '/node_modules/.bin/browser-refresh';
    if (!file_exists($executable)) {
      $executable = $this->exec('which browser-refresh');
      if (empty($executable)) {
        $this->displayMessage($this->t('Main application browser-refresh is not available!'), 'error');
        $this->displayMessage($this->t('Try calling this: npm install -g browser-refresh'), 'status');
        return;
      }
    }

    // Check if browser-refresh-client is installed
    if ($this->exec('node br.js check') !== 'OK!') {
      $this->displayMessage($this->t('Required node modules are not installed!'), 'error');
      $this->displayMessage($this->t('Try calling this: npm install -g browser-refresh-client'), 'status');
      return;
    }

    // Write configuration for the new process.
    $config_filename = DRUPAL_ROOT . '/' . drupal_get_path('module', 'browser_refresh') . '/.browser-refresh';
    $temp_filename = $this->fileSystem->realpath($this->fileSystem->tempnam('temporary://', 'browser-refresh-'));
    $params = array(
      'watch' => array(DRUPAL_ROOT),
      'urlFileName' => $temp_filename,
    );
    file_put_contents($config_filename, json_encode($params));

    // Start the process.
    $pid = (int) $this->exec('nohup ' . $executable . ' > /dev/null 2>&1 & echo $!');
    if ($pid && file_exists($temp_filename)) {
      $url = '';
      while (empty($url)) {
        sleep(1);
        $url = file_get_contents($temp_filename);
      }
      $this->configFactory->getEditable('browser_refresh.settings')
        ->set('url', $url)
        ->set('pid', $pid)
        ->save(TRUE);
    }
    unlink($config_filename);
    unlink($temp_filename);

    $this->isActive(TRUE);
    $this->displayMessage('You should refresh your browser to see the updated status.');
  }

  /**
   * {@inheritdoc}
   */
  public function stop(): void {
    if (!$this->isActive()) {
      $this->displayMessage('Browser Refresh is not running.', 'error');
      return;
    }

    $pid = $this->getPid();
    $this->configFactory->getEditable('browser_refresh.settings')
      ->set('url', '')
      ->set('pid', 0)
      ->save(TRUE);
    $this->exec('kill ' . $pid);
    $this->displayMessage('Browser Refresh stopped.');
    $this->displayMessage('You should refresh your browser to see the updated status.');
  }

  /**
   * {@inheritdoc}
   */
  public function restart(): void {
    $this->stop();
    $this->start();
  }

  /**
   * Callback to execute a command line command from the module's directory
   * and to return the given line of output.
   *
   * @param $command
   * @param int $line
   *
   * @return string
   */
  private function exec($command, $line = 0): string {
    chdir(DRUPAL_ROOT . '/' . drupal_get_path('module', 'browser_refresh'));

    $op = array();
    exec($command, $op);
    return empty($op[$line]) ? '' : $op[$line];
  }

  /**
   * Callback to output a message. If called by Drush a messenger message is
   * invoked, otherwise we write to the OutputInterface from DrupalConsole.
   *
   * @param $msg
   * @param string $type
   */
  private function displayMessage($msg, $type = MessengerInterface::TYPE_STATUS): void {
    if (empty($this->output)) {
      $this->messenger->addMessage($msg, $type);
    }
    else {
      $this->output->writeln($msg);
    }
  }

}
